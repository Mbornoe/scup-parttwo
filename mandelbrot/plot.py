"""
This file contains functionalities for plotting all the
required plots defined in the mini-project
"""
import matplotlib.pyplot as plt
import filehandling as fh
import utility


def plot_mandelbrot_array(plotName, matrix):
    """
    Funtion that plots the mandelbrot simulation result

    Args:
        plotName: Case dependend plot name
        matrix: The desired result matrix
    """
    config = fh.load_configfile("../configuration/config.json")
    p_re = config['defaultSettings']['p_re']
    p_im = config['defaultSettings']['p_im']
    re_lower_limit = config['defaultSettings']['re_lower_limit']
    re_upper_limit = config['defaultSettings']['re_upper_limit']
    im_lower_limit = config['defaultSettings']['im_lower_limit']
    im_upper_limit = config['defaultSettings']['im_upper_limit']

    plt.imshow(matrix, interpolation='bilinear', cmap=plt.cm.hot, origin='lower', extent=[
               re_lower_limit, re_upper_limit, im_lower_limit, im_upper_limit])

    plt.savefig(plotName + '.pdf', format='pdf')
    # plt.show()


def plot_parallel_performance(plotName, timings):
    """
    Funtion that plots the mandelbrot simulation result

    Args:
        plotName: Case dependend plot name
        matrix: The desired result matrix
    """
    xLabel = []
    for i in range(1,len(timings)+1):
        xLabel.append(i) 
 
    plt.plot(xLabel, timings)
    plt.ylabel('Seconds')
    plt.xlabel('Computational units')
    plt.title(
        'Execution time versus number of Computational units')
    #plt.xlim(1, len(timings))
    plt.savefig(plotName + 'speedUp.pdf', format='pdf')

"""
This module is "main" which the user can use to 
make the simulation that creates the required plots
for this project. 
"""
import numpy as np
import time

import mandelbrot as mb
import filehandling as fh
import plot as plot
import utility
import argparse


def simulate_all(args):
    """
    Caretakes simulation and plotting of all the required
    simulations and plots defined in the mini-project
    """

    methods = [
        'mb.mandelbrot_naive(complex_matrix, threshold, iterations)',
        'mb.mandelbrot_numba(complex_matrix, threshold, iterations)',
        'mb.mandelbrot_parallel(complex_matrix, threshold, iterations, parallelUnits)'
    ]

    methodName = [
        'Mandelbrot_naive',
        'Mandelbrot_numba',
        'Mandelbrot_parallel'
    ]

    config = fh.load_configfile(args.config)
    p_re = config['defaultSettings']['p_re']
    p_im = config['defaultSettings']['p_im']
    re_lower_limit = config['defaultSettings']['re_lower_limit']
    re_upper_limit = config['defaultSettings']['re_upper_limit']
    im_lower_limit = config['defaultSettings']['im_lower_limit']
    im_upper_limit = config['defaultSettings']['im_upper_limit']
    threshold = config['defaultSettings']['threshold']
    iterations = config['defaultSettings']['iterations']

    parallelUnits = config['parallelSettings']['numberOfUnits']

    for m, mName in zip(methods, methodName):
        complex_matrix = utility.create_complex_matrix(p_re, p_im, re_lower_limit,
                                   re_upper_limit, im_lower_limit, im_upper_limit)

        y = eval(m)  # Introduced for numba JIT
        tic = time.time()
        y = eval(m)
        toc = time.time() - tic
        print('{:30s} : {:10.2e} [s]'.format(mName, toc))

        tic = time.time()
        fh.save_to_file(fh.create_case_filepath(mName), y)
        toc = time.time() - tic
        # print('{:30s} :Saving data {:10.2e} [s]'.format(mName, toc))

        tic = time.time()
        plot.plot_mandelbrot_array(fh.create_case_filepath(mName) + mName, y)
        toc = time.time() - tic
        # print('{:30s} :Plotting {:10.2e} [s]'.format(mName, toc))


def parallel_speed_up(args):
    """
    Caretakes simulation and plotting of the Parallel
    implementation execution time speed up versus
    number of Computational units
    """
    config = fh.load_configfile(args.config)
    p_re = config['defaultSettings']['p_re']
    p_im = config['defaultSettings']['p_im']
    re_lower_limit = config['defaultSettings']['re_lower_limit']
    re_upper_limit = config['defaultSettings']['re_upper_limit']
    im_lower_limit = config['defaultSettings']['im_lower_limit']
    im_upper_limit = config['defaultSettings']['im_upper_limit']
    threshold = config['defaultSettings']['threshold']
    iterations = config['defaultSettings']['iterations']

    parallelUnits = config['parallelSettings']['numberOfUnits']

    timing = []

    for i in range(1, parallelUnits + 1):
        complex_matrix = utility.create_complex_matrix(p_re, p_im, re_lower_limit,
                                                       re_upper_limit, im_lower_limit, im_upper_limit)
        tic = time.time()
        mb.mandelbrot_parallel(complex_matrix, threshold, iterations, i)
        toc = time.time() - tic
        timing.append(toc)
        print('Processing units: {:30d} : {:10.2e} [s]'.format(i, toc))

    plot.plot_parallel_performance(
        fh.create_case_filepath("Mandelbrot_parallel"), timing)


def main(args):
    """
    Based on input args, this function will execute the
    desired functions.
    """

    if args.all:
        simulate_all(args)

    if args.parallel:
        parallel_speed_up(args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This program takes a json formatted\
     configuration file and simulates Mandelbrot sets.\
     The configuration file must contain appropiate default parameters\
     aswell as test cases. When executing the simulation,\
     the files will be stored in belonging cases depended folder in the\
     "cases" directory.\n\n \
     Example of use:\n \
     python benchmark.py -c "../configuration/config.json" -a \n\
     python benchmark.py -c "../configuration/config.json" -p',
        epilog='Developed in summer 2016 by Morten Bornoe Jensen for the\
     Scientific Computing using Pythong - Part Two\n',
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-c', '--config', metavar='"../configuration/config.json"',
                        default='"../configuration/config.json"',
                        type=str, help='Path to the json formatted config file.')
    parser.add_argument('-a', '--all', action='store_true',
                        help='Do all simulations')
    parser.add_argument('-p', '--parallel', action='store_true',
                        help='Do parallel simulation')

    args = parser.parse_args()
    main(args)

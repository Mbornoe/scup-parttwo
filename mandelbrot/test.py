"""
This will be our test to make sure we don not make a faster but erroneous function!

"""

import unittest
from scipy.linalg import toeplitz
import numpy as np
import filehandling as fh
import utility
import mandelbrot as mb


class TestCase(unittest.TestCase):

    def setUp(self):
        # This will contain a list of the methods we will test
        self.methods = [
            'mb.mandelbrot_naive(complex_matrix, threshold, iterations)',
            'mb.mandelbrot_numba(complex_matrix, threshold, iterations)',
            'mb.mandelbrot_parallel(complex_matrix, threshold, iterations, parallelUnits)'
        ]

    def test_mandelbrot_implementations(self):
        p_re = 10
        p_im = 10
        re_lower_limit = -2.0
        re_upper_limit = 1.0
        im_lower_limit = -1.5
        im_upper_limit = 1.5

        results = []
        complex_matrix = utility.create_complex_matrix(p_re, p_im, re_lower_limit,
                                                       re_upper_limit, im_lower_limit, im_upper_limit)
        threshold = 10
        iterations = 100
        parallelUnits = 8
        for m in self.methods:
            y = eval(m)
            results.append(y)

        for i in range(1, len(results)):
            self.assertEqual(results[i], results[i - 1])

    def test_mandelbrot_compute_naive(self):
        threshold = 10
        iterations = 100
        parallelUnits = 8

        # Checking mandelbrot computer
        self.assertAlmostEqual(mb.mandelbrot_naive_number_compute(
            complex(0, 0), threshold, iterations), 1)

    def test_mandelbrot_compute_numba(self):
        threshold = 10
        iterations = 100
        parallelUnits = 8

        # Checking mandelbrot computer
        self.assertAlmostEqual(mb.mandelbrot_numba_number_compute(
            complex(0, 0), threshold, iterations), 1)

    def test_mandelbrot_compute_parallel(self):
        threshold = 10
        iterations = 100
        parallelUnits = 8

        # Checking mandelbrot computer
        self.assertAlmostEqual(mb.mandelbrot_parallel_number_compute(
            complex(0, 0), threshold, iterations), 1)

    def test_ulility_load_config(self):
        config = fh.load_configfile("../configuration/config.json")
        p_re = config['defaultSettings']['p_re']
        p_im = config['defaultSettings']['p_im']
        re_lower_limit = config['defaultSettings']['re_lower_limit']
        re_upper_limit = config['defaultSettings']['re_upper_limit']
        im_lower_limit = config['defaultSettings']['im_lower_limit']
        im_upper_limit = config['defaultSettings']['im_upper_limit']
        threshold = config['defaultSettings']['threshold']
        iterations = config['defaultSettings']['iterations']

        parallelUnits = config['parallelSettings']['numberOfUnits']

        # Checking loaded variables are not empty
        self.assertTrue(p_re)
        self.assertTrue(p_im)
        self.assertTrue(re_lower_limit)
        self.assertTrue(re_upper_limit)
        self.assertTrue(im_lower_limit)
        self.assertTrue(im_upper_limit)
        self.assertTrue(threshold)
        self.assertTrue(iterations)
        self.assertTrue(parallelUnits)

        # Checking the limits
        self.assertLess(re_lower_limit, re_upper_limit)
        self.assertLess(im_lower_limit, im_upper_limit)

        # Dimensions of real and complex matrices must be equal
        self.assertEqual(p_re, p_im)

        # Checking that iterations are more than 2
        self.assertGreaterEqual(iterations, 2)

        # Checking that compution units are more than 1
        self.assertGreaterEqual(parallelUnits, 2)


if __name__ == '__main__':
    unittest.main()

"""
Methods for compution Mandelbrot sets

"""
import numpy as np
import itertools
from numba import jit
from multiprocessing import Pool, freeze_support

# No-op for use with profiling and test
try:
    @profile
    def f(x): return x
except:
    def profile(func):
        def inner(*args, **kwargs):
            return func(*args, **kwargs)
        return inner


def mandelbrot_naive(complex_matrix, threshold, iterations):
    """
    Naive version of computing Mandelbrot sets.

    Args:
        complex_matrix: The entire complex matrix
                        (e.g. utility.create_complex_matrix)
        threhsold: The threshold value the simulation must be within
        iterations: Number of iterations to perfom
    """

    workingList = []
    array = []

    for row in complex_matrix:
        workingList = []

        for cell in row:
            mapped_number = mandelbrot_naive_number_compute(
                cell, threshold, iterations)
            workingList.append(mapped_number)

        array.append(workingList)

    return array


def mandelbrot_naive_number_compute(complex_number, threshold, iterations):
    """
    Function that simulates Mandelbrot for a given/known C.

    Args:
        complex_number: A specifc number from the Complex Matrix
        threhsold: The threshold value the simulation must be within
        iterations: Number of iterations to perfom
    """
    complex_z = complex_number
    for i in range(0, iterations):
        complex_z = complex_z**2 + complex_number
        if abs(complex_z) > threshold:
            return float(i + 1) / float(iterations)
    return 1


@jit
def mandelbrot_numba(complex_matrix, threshold, iterations):
    """
    Numba version of computing Mandelbrot set

    Args:
        complex_matrix: The entire complex matrix
                        (e.g. utility.create_complex_matrix)
        threhsold: The threshold value the simulation must be within
        iterations: Number of iterations to perfom
    """
    complex_matrix_NP = np.array(complex_matrix)
    working_matrix = np.zeros(shape=complex_matrix_NP.shape)
    [rows, cols] = working_matrix.shape

    for i in range(0, rows):
        for j in range(0, cols):
            working_matrix[i, j] = mandelbrot_numba_number_compute(
                complex_matrix_NP[i, j], threshold, iterations)

    return np.array(working_matrix).tolist()


@jit
def mandelbrot_numba_number_compute(complex_number, threshold, iterations):
    """
    Numba version of simulating the Mandelbrot set

    Args:
        complex_matrix: The entire complex matrix
                        (e.g. utility.create_complex_matrix)
        threhsold: The threshold value the simulation must be within
        iterations: Number of iterations to perfom
    """
    complex_z = complex_number
    for i in range(0, iterations):
        complex_z = complex_z**2 + complex_number
        if abs(complex_z) > threshold:
            return float(i + 1) / float(iterations)
    return 1


def mandelbrot_parallel(complex_matrix, threshold, iterations, parallelUnits):
    """
    Parallel version of computing Mandelbrot set

    Args:
        complex_matrix: The entire complex matrix
                        (e.g. utility.create_complex_matrix)
        threhsold: The threshold value the simulation must be within
        iterations: Number of iterations to perfom
        parallelUnits: Number of processing units
    """
    pool = Pool(processes=parallelUnits)

    converted_complex = np.array(complex_matrix)
    dim = converted_complex.shape[0]
    # change into one long list
    converted_complex = np.reshape(converted_complex, converted_complex.size)
    mandelbrot_array = np.zeros((dim, dim), dtype=np.double)
    array = pool.map(mandelbrot_parallel_number_compute_star, itertools.izip(
        converted_complex, itertools.repeat(threshold), itertools.repeat(iterations)), chunksize=125)

    array = np.reshape(array, [dim, dim])
    pool.close()
    return np.array(array).tolist()


def mandelbrot_parallel_number_compute(complexNumber, threshold, iterations):
    """
    Function that simulates Mandelbrot for a given/known C.

    Args:
        complexNumber: A specifc number from the Complex Matrix
        threhsold: The threshold value the simulation must be within
        iterations: Number of iterations to perfom
    """
    complex_z = complexNumber
    for i in range(0, iterations):
        complex_z = complex_z**2 + complexNumber
        if abs(complex_z) > threshold:
            return float(i + 1) / float(iterations)
    return 1


def mandelbrot_parallel_number_compute_star(complexNumber_threshold_iterations):
    """
    Convert `f([complexNumber, threshold, iterations])`
    to `f(complexNumber, threshold, iterations)` call.

    Args:
        complexNumber: A specifc number from the Complex Matrix
        threhsold: The threshold value the simulation must be within
        iterations: Number of iterations to perfom
    """
    return mandelbrot_parallel_number_compute(*complexNumber_threshold_iterations)

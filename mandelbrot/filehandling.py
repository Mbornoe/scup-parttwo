"""
This file can contain functionalities for saving/loading data

"""
import json
import os


def load_configfile(configPath):
    """
    Function that loads json formatted config file

    Args:
        configPath: Path to configuration file
    """
    with open(configPath, 'r') as f:
        config = json.load(f)
    return config


def print_config(config):
    """
    Function for printing the allCases dictionaries.

    Args:
        config: An allCases dictionaries (e.g. output from sort_config)
    """
    if type(config) == dict:
        for keys, values in config.items():
            print '%s : %s' % (keys, values)
    else:
        print config


def create_case_filepath(caseDirectory):
    """
    Function for creating case filepath name

    Args:
        caseDirectory: String containing info of case
    """
    workingDir = "../cases/" + caseDirectory + "/"
    if not os.path.isdir(workingDir):
        os.makedirs(workingDir)

    return workingDir


def save_to_file(filename, simulation):
    """
    Function saving results in one json formatted file.
    The file is saved into the specific case folder.

    Args:
        filename: Desired filename
        simulation: Numpy array with simulation results
    """
    results = {
        'z': list(simulation)
    }
    if not os.path.isdir(filename):
        os.makedirs(filename)

    with open((filename + '/data.json'), 'w') as fp:
        json.dump(results, fp, indent=4)


def load_json_results(path):
    """
    Function that loads results from one json formatted file.

    Args:
        path: Directory path of desired json file.
    """
    results = json.loads(open((path + '/data.json')).read())
    return results['z']

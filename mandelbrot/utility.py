"""
Utility methods for compution mandelbrot sets

"""
import numpy as np
from numba import jit


def create_complex_matrix(p_re, p_im, re_lower_limit,
                          re_upper_limit, im_lower_limit, im_upper_limit):
    """
    Function that are used to create generate the real and imaginary/complex
    matrix, C, that are used for as input to the mandelbrot simulation.

    Args:
        p_re: Dimension of the real matrix.
        p_im: Dimension of the complex matrix.
        re_lower_limit: Lower boundray for the real matrix
        re_upper_limit: Higher boundray for the real matrix
        im_lower_limit: Lower boundray for the complex matrix
        im_upper_limit: Upper boundray for the complex matrix
    """
    return [[re_i+im_i*1j for re_i in np.linspace(re_lower_limit,
            re_upper_limit, p_re, endpoint=True)] for im_i in
            np.linspace(im_upper_limit, im_lower_limit, p_im, endpoint=True)]

# Scientific Computing using Python, part 2 mini-project
A repository containing the mini-project for the PhD course "Scientific Computing using Python, part 2", held at Aalborg University, May 2016. 

# Introduction
This python project relates to the Mandelbrot set and the belonging simulating. The simulation is done by in 3 different python implementations, namely naive, numba, and parallel.

# Prerequisites
If you pull the entire project, you should be able to execute below code examples. If you however wish to mange changes to configuration, you have to pay attention to the config.json file located in the configuration folder. There are 9 different parameteres that must be defined in this configuration, these are:

* p_re : Dimension of the real matrix.
* p_im : Dimension of the complex matrix.
* re_lower_limit : Lower boundray for the real matrix
* re_upper_limit : Higher boundray for the real matrix
* im_lower_limit : Lower boundray for the complex matrix
* im_upper_limit : Upper boundray for the complex matrix
* threshold : The threshold value the mandelbrot set simulation must be within
* iterations : Number of iterations to perfom for each mandelbrot set
* numberOfUnits : The number of computational units for parallel implementation


As previously mentioned, these are wrapped into the json config file, which must meet the standard shown in below example.

```python
{
    "defaultSettings": {
        "p_re":1000,
        "p_im":1000,
        "re_lower_limit": -2.0,
        "re_upper_limit": 1.0,
        "im_lower_limit": -1.5,
        "im_upper_limit": 1.5,
        "threshold": 10,
        "iterations": 100
    },
    "parallelSettings": {
        "numberOfUnits": 8
    }
}



```

# How to run it
There are three flags when wanting to run the simultations for this mini-project.

* -c : Path to the json formatted config file.
* -a : Simulate all the mandelbrot simulations
* -p : Use parallel implementation to compare execution time speed up versus
    number of Computational units

Examples of how to run the mini-project are seen below.

```python
# Executes all three mandelbrot implementations and plots
python benchmark.py -c "../configuration/config.json" -a

# Executes the parallel mandelbrot with increasing proccssing units and plots
python benchmark.py -c "../configuration/config.json" -p
```
# Tests

The unittest can be executed by executing test.py.
```python
# Executes unittest
python test.py
```

# Contributors 
The code is developed by Morten Bornø Jensen from Aalborg University in summer 2016.

